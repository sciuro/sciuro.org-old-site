<b>Install Debian testing on an iBook G4</b>

Fact one, I still have an iBook G4 and little use to it. Fact two, sometimes I
still miss my linux desktop. (Mac OS X is not that bad) Combine those two facts
and we are going to install Debian Testing on an iBook G4.

<b>Getting the software</b>
I used the netinstaller from
http://cdimage.debian.org/cdimage/daily-builds/daily/arch-latest/powerpc/iso-cd/
Burn the debian-testing-powerpc-netinst.iso to a CD. (I know, it's an old medium)

<b>Do an installation</b>
Boot your iBook and insert the CD. Press 'c' and hold while or before
Apple-Boot-Sound to start from CD. Maybe you have to boot it again if you are too
slow. He asks you what you want to do. type

  install video=ofonly

The debian installer doesn't like the iBook's graphic card. Proceed like a normal
Debian installation, until the disk layout.

<b>Disk partitioning</b>
Choose for 'guided partitioning'. Then you will get an reconfigured partitionschema
which you can change. You will see an extra partition, the 'Apple' type partition.
Change your disk partition like you want, but keep the first Apple partition!!!
Otherwise it can't format the drive. I did it my way the first time and had some
problems formatting my drive. Maybe it's a bug in testing, but I couldn't find the
Apple type, but it worked using the 'guided partitioning'.

This is my partition table:

  IDE1 master (hda) - 120.0 GB FUJITSU MHV2120AT
  #1 32.3 kB Apple
  #2 1.0 MB B K boot untitled
  #3 7.0 GB f ext4 untitled /
  #4 3.5 GB f swap swap swap
  #5 109.6 GB f ext4 untitled /home

If you got the error 'Partition map has no partition map entry' after writing the
partition table to disk, you forgot to make the first Apple partition.

Your system will reboot.

Choose the parts you want to install. I simply only choose to install basics and
laptop. All the rest I will do by myself.

Reboot into your new OS. The first thing I noticed was the fan. It sounds like a
broken fan. After about 5 minutes it scaled down to zero and the noise was gone.

<b>Console tools</b>
Just aptitude install gpm. This will install gpm, the mouse interface for the
console. An USB mouse will work out of the box.

<b>X</b>
Installing X is a bit strange in Debian because of the Radeon Mobility 9200. Main
problem is the framebuffer loaded at boot time. The next can be done by every
windowmanager, but I prefer KDE. Make sure your /etc/apt/sources.list contains
besides main, also the contrib and non-free repo's.

  aptitude install xorg
  aptitude install kde-standard
  aptitude install firmware-linux

Do not reboot now! It will freeze during boot. Add the following into your
/etc/yaboot.conf:

  image=/boot/vmlinux
  label=Linux
  read-only
  initrd=/boot/initrd.img
  append="video=ofonly video=radeonfb:off radeon.agpmode=-1"

  image=/boot/vmlinux.old
  label=old
  read-only
  initrd=/boot/initrd.img.old
  append="video=ofonly video=radeonfb:off radeon.agpmode=-1"

Then run:

  root@thuu:~# ybin -v
  ybin: Finding OpenFirmware device path to `/dev/hda2'...
  ybin: Installing first stage bootstrap /usr/lib/yaboot/ofboot onto /dev/hda2...
  ybin: Installing primary bootstrap /usr/lib/yaboot/yaboot onto /dev/hda2...
  ybin: Installing /etc/yaboot.conf onto /dev/hda2...
  ybin: Setting attributes on ofboot...
  ybin: Setting attributes on yaboot...
  ybin: Setting attributes on yaboot.conf...
  ybin: Blessing /dev/hda2 with Holy Penguin Pee...
  ybin: Updating OpenFirmware boot-device variable in nvram...

Now you may reboot. If you accidentally reboot your system before changing
yaboot and you are stuk at the message:

  conflicting fb hw usage radeondrmfb

Give during boot time in the yaboot menu the command:

  boot: Linux video=radeonfb:off radeon.agpmode=-1

It will boot again with the right options.

<b>Wireless</b>
First, make sure in your /etc/apt/sources.list contains the contrib packages.
Update your repository and install the package b43-fwcutter. Download the
firmware for the b43 from

  http://downloads.openwrt.org/sources/broadcom-wl-4.150.10.5.tar.bz2

Untar the broadcom code and cd into the driver directory

  b43-fwcutter -w /lib/firmware/ wl_apsta_mimo.o

And your Wireless is working. (can somebody test this again? Maybe I missed
something)

<b>Bluetooth</b>
Bluetooth is working out of the box. Install btscanner to see it for yourself.
Config files are in /etc/bluetooth.
