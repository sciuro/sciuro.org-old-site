<b>Getting Wolfenstein (wolf) running in Debian</b>

Running games in Linux could be a pain in the ass. Here I tried to install
Wolfenstein.

Install your software. Probebly it goes to /usr/local/games/wolfenstein. And
of course make your openGL working. See the big internet for doing this.

If you start your game (./wolf) it will come up with the following error:

Wolf 1.1b linux-i386 Jan 17 2002
----- FS_Startup -----
Current search path:
/home/rogier/.wolf/main
/usr/local/games/wolfenstein/main/mp_pakmaps0.pk3 (21 files)
/usr/local/games/wolfenstein/main/mp_pak2.pk3 (3 files)
/usr/local/games/wolfenstein/main/mp_pak1.pk3 (308 files)
/usr/local/games/wolfenstein/main/mp_pak0.pk3 (783 files)
/usr/local/games/wolfenstein/main/pak0.pk3 (4775 files)
/usr/local/games/wolfenstein/main
./wolf.x86/main

----------------------
5890 files in pk3 files
execing default.cfg
couldn't exec language.cfg
couldn't exec wolfconfig_mp.cfg
couldn't exec autoexec.cfg
Hunk_Clear: reset the hunk ok
Joystick is not active.
Bypassing CD checks
----- Client Initialization -----
----- Initializing Renderer ----
-------------------------------
Loaded 714 translation strings from scripts/translation.cfg
----- Client Initialization Complete -----
----- R_Init -----
...loading libGL.so: QGL_Init: Can't load libGL.so from /etc/ld.so.conf
or current dir: /usr/local/games/wolfenstein/libGL.so: cannot open shared
object file: No such file or directory
failed
...loading libMesaVoodooGL.so.3.1: QGL_Init: Can't load libMesaVoodooGL.so.3.1
from /etc/ld.so.conf or current dir:
/usr/local/games/wolfenstein/libMesaVoodooGL.so.3.1: cannot open shared object
file: No such file or directory
failed
----- CL_Shutdown -----
RE_Shutdown( 1 )
-----------------------
----- CL_Shutdown -----
-----------------------
Sys_Error: GLimp_Init() - could not load OpenGL subsystem

<b>Solution</b>
There is nothing wrong, but Debian is missing a link. So make the link:

cd /usr/lib32/
ln -s libGL.so.1 libGL.so
