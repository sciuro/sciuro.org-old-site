<b>How to split a big file in smaler ones</b>

Sometimes you got the problem that a file is something too big. No problem. There
is a solution for that. The command split.

Split a file
How do we split a file? Simple. To split the file bigfile into smaller files from
100 MB with the name smallfile00, smallfile01, smallfile02 etc. we do:

  split -d -b100m bigfile smallfile

If you want an extention, some people or program's like that, you can give them
an extention:

  for i in `ls smallfile*` do mv $i $i.abc; done

And you got all smaller files.

Join a file
Later on, to join the files together, you can use the command cat.

  cat smallfile* > bigfile
