<b>backup and restoring partitions</b>

You always wonder how to make a backup of a complete disk? For example to rescue
the disk, or you want to save te data for later on? Or mayby to make an image
and distribute it over a lot of computers? Here is how.

Make a backup of a partition (this could also be the whole disk):

One-to-one:
  dd if=/dev/hdx1 of=/path/to/image

To a compressed image:
  dd if=/dev/hdx2 | gzip > /path/to/image.gz

To mount the partition on a local directory:
  mount -o loop /path/to/image /mnt

To restore a partition (first make sure the partition on disk is the same size or
bigger than the original):

One-to-one:
  dd if=/path/to/image of=/dev/hdx1

From a compressed image:
  gzip -dc /path/to/image.gz |dd of=/dev/hdx2
