<b>About</b>
<img src='__SITEURL__/images/foto_me.jpg' align='right'>
My name is Rogier, or Roger as most non-dutch speakers
will prefer. I am a 33 year old computer engineer,
living in The Netherlands.

Besides being a computer nerd, I spend my time with
Scouting, Geocaching, hiking and biking. And sometimes
climbing and skiing.

<b>The name Sciuro</b>
The name Sciuro is a nickname I got from some Swiss
scouts. In Switserland it's common to give every scout
a nickname and they came up with the nickname Sciuro.
It's pronounced as t.si.uro and meaning 'squirrel' in
Esperanto. If you don't know what esperanto is, <a href='http://s.sciuro.org/zcm6w' target='new'>here</a>
is an explaination.







